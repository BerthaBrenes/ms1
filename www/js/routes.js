angular.module('app.routes', [])

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider
    

      .state('bienvenido', {
    url: '/page2',
    templateUrl: 'templates/bienvenido.html',
    controller: 'bienvenidoCtrl'
  })

  .state('conceptosBSicosDeLaResistenciaDeMateriales', {
    url: '/page3',
    templateUrl: 'templates/conceptosBSicosDeLaResistenciaDeMateriales.html',
    controller: 'conceptosBSicosDeLaResistenciaDeMaterialesCtrl'
  })

  .state('fuerzasCortantesYMomentoFlexionantesEnVigas', {
    url: '/page10',
    templateUrl: 'templates/fuerzasCortantesYMomentoFlexionantesEnVigas.html',
    controller: 'fuerzasCortantesYMomentoFlexionantesEnVigasCtrl'
  })

  .state('esfuerzosYDeformacionesEnVigas', {
    url: '/page17',
    templateUrl: 'templates/esfuerzosYDeformacionesEnVigas.html',
    controller: 'esfuerzosYDeformacionesEnVigasCtrl'
  })

  .state('solicitacionesEspeciales', {
    url: '/page18',
    templateUrl: 'templates/solicitacionesEspeciales.html',
    controller: 'solicitacionesEspecialesCtrl'
  })

  .state('tab', {
    url: '/page1',
    templateUrl: 'templates/tab.html',
    abstract:true
  })

  .state('teorA', {
    url: '/page13',
    templateUrl: 'templates/teorA.html',
    controller: 'teorACtrl'
  })

  .state('teorA2', {
    url: '/page6',
    templateUrl: 'templates/teorA2.html',
    controller: 'teorA2Ctrl'
  })

  .state('fRmulas', {
    url: '/page14',
    templateUrl: 'templates/fRmulas.html',
    controller: 'fRmulasCtrl'
  })

  .state('fRmulas2', {
    url: '/page7',
    templateUrl: 'templates/fRmulas2.html',
    controller: 'fRmulas2Ctrl'
  })

  .state('ejercicios', {
    url: '/page15',
    templateUrl: 'templates/ejercicios.html',
    controller: 'ejerciciosCtrl'
  })

  .state('ejercicios2', {
    url: '/page8',
    templateUrl: 'templates/ejercicios2.html',
    controller: 'ejercicios2Ctrl'
  })

  .state('videos', {
    url: '/page16',
    templateUrl: 'templates/videos.html',
    controller: 'videosCtrl'
  })

  .state('videos2', {
    url: '/page9',
    templateUrl: 'templates/videos2.html',
    controller: 'videos2Ctrl'
  })

  .state('teorA3', {
    url: '/page11',
    templateUrl: 'templates/teorA3.html',
    controller: 'teorA3Ctrl'
  })

  .state('fRmulas3', {
    url: '/page12',
    templateUrl: 'templates/fRmulas3.html',
    controller: 'fRmulas3Ctrl'
  })

  .state('ejercicios3', {
    url: '/page19',
    templateUrl: 'templates/ejercicios3.html',
    controller: 'ejercicios3Ctrl'
  })

  .state('teorA4', {
    url: '/page20',
    templateUrl: 'templates/teorA4.html',
    controller: 'teorA4Ctrl'
  })

$urlRouterProvider.otherwise('')


});